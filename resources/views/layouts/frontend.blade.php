<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Maven Digital Asia</title>
    <meta name="author" content="Maven" />
    <meta name="description" content="Maven Jakarta + Singapore" />
    <meta name="keywords"
        content="pembuatan aplikasi, jasa aplikasi android, digital agency indonesia, website developers, website developer, web programer, ui ux design agency, design agency, jasa design website" />
    <meta name="Resource-type" content="Document" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1" />
    <!-- Stylesheets-->
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('assets/frontend/img/favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('assets/frontend/img/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('assets/frontend/img/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ url('assets/frontend/img/favicons/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ url('assets/frontend/img/favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116065543-1"></script>-->
    <!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-180995992-1"></script>-->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-521398885"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'AW-521398885');
    </script>
    <!--<script>-->
    <!-- 	window.dataLayer = window.dataLayer || [];-->
    <!-- 	function gtag(){dataLayer.push(arguments);}-->
    <!-- 	gtag('js', new Date());-->
    <!-- 	gtag('config', 'UA-116065543-1');-->
    <!--	gtag('config', 'UA-180995992-1');-->
    <!--</script>-->
    <!--<script>-->
    <!-- !function(f,b,e,v,n,t,s)-->
    <!-- {if(f.fbq)return;n=f.fbq=function(){n.callMethod?-->
    <!-- n.callMethod.apply(n,arguments):n.queue.push(arguments)};-->
    <!-- if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';-->
    <!-- n.queue=[];t=b.createElement(e);t.async=!0;-->
    <!-- t.src=v;s=b.getElementsByTagName(e)[0];-->
    <!-- s.parentNode.insertBefore(t,s)}(window, document,'script',-->
    <!-- 'https://connect.facebook.net/en_US/fbevents.js');-->
    <!-- fbq('init', '1845687818989309');-->
    <!-- fbq('track', 'PageView');-->
    <!--</script>-->
    <!--<noscript>-->
    <!--    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1845687818989309&ev=PageView&noscript=1"/>-->
    <!--</noscript>-->

    <!-- {{ url('assets/frontend/css/bootstrap.css') }} -->

    <link rel="stylesheet" href="{{ url('assets/frontend/plugins/fullpage/jquery.fullpage.css') }}" />
    <link rel="stylesheet" href="{{ url('assets/frontend/css/bootstrap.min.css') }}">
    <link rel="preload" as="style" href="{{ url('assets/frontend/css/font-awesome.all.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/plugins/bottom-sheet/bottom-sheet.css') }}">
    <link rel="preload" as="style" href="{{ url('assets/frontend/plugins/slick/slick.css') }}">
    <link rel="preload" as="style" href="{{ url('assets/frontend/plugins/slick/slick-theme.css') }}">
    @stack('styles')
</head>

<body class="stretched">
    <div id="wrapper" class="clearfix">
        @include('parts.frontend.header')
        @yield('content')
        @include('parts.frontend.footer')
    </div>
    {{-- Cookir Consent --}}
    <div id="cookieConsent">
        <div id="closeCookieConsent">x</div>
        This website is using cookies. <a class="cookieConsentOK">That's Fine</a>
    </div>
    <!-- #wrapper end -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" defer>
    </script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery.session@1.0.0/jquery.session.min.js">
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.5.4/umd/popper.min.js" defer>
    </script>
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" defer>
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"
        defer></script>
    <script type="text/javascript" src="{{ url('assets/frontend/plugins/scrolloverflow.min.js') }}" defer></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.min.js"
        defer></script>
    <script type="text/javascript" src="{{ url('assets/frontend/plugins/bottom-sheet/bottom-sheet.js') }}" defer>
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" defer>
    </script>
    <script type="text/javascript" src="{{ url('assets/frontend/plugins/typed/typed.min.js') }}" defer></script>

    <script type="text/javascript" src="{{ url('assets/frontend/plugins/limarquee/jquery.limarquee.js') }}" defer>
    </script>
    @stack('scripts')
</body>

</html>