<style>
  .wa-custom {
    display: block;
    position: fixed; 
    right: 20px; 
    bottom: 20px; 
    width: 50px; 
    height: 50px;
    z-index: 100;
  }
  .wa-custom img {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    object-fit: cover;
    object-position: center;
  }
</style>
<a href="javascript:void(0)" class="wa-custom">
  <img src="{{ url('assets/frontend/img/whatsapp.png') }}">
</a>
<div class="scroll-icon animated">
  <!-- <p class="scroll-text">Scroll For More Info</p> -->
  <a class="smoothscroll">
      <div class="mouse"></div>
  </a>
  <div class="end-top"></div>
</div>