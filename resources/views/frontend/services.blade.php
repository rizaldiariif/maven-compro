@extends('layouts.frontend')

@push('styles')
<link rel="stylesheet" href="{{ url('assets/frontend/css/custom.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="{{ url('assets/frontend/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/frontend/js/services.js') }}"></script>
@endpush

@section('content')
<div id="fullpage">
    <div class="section " id="section0">
        <div class="container-custom">
            <div class="static-height-div">
                <div class="typing-title-container animated">
                    <span class="typing-title" id="typing-services">We <br class="title-new-line">CREATE
                        <br>WORLD-CLASS</span>
                </div>
                <p class="animation-in animated">ROI driven Digital Products. only by crafting
                    <br>through amazing well thought out disclipines, see
                    <br>below to get a feel of what we can help you with.</p>
            </div>
        </div>
        <div class="number-home animated">02</div>
    </div>
    <div class="section" id="section1">
        <!-- <img src="{{ url('assets/frontend/img/left-round.svg') }}" style="position: absolute;left: 0;top: 0;height: 100%"> -->

        <div class="bg-wrap animated">
            <div class="bg-slide-services2 animated"></div>
            <div class="top-btn">
                <a href="#" id="vertical-prev"><i class="fas fa-chevron-up"></i></a>
            </div>
            <div class="bottom-btn">
                <a href="#" id="vertical-next"><i class="fas fa-chevron-down"></i></a>
            </div>
        </div>
        <div class="h-100 slide-vertical-left">
            <div class="row justify-content-center align-items-center h-100">
                <div class="col-lg-5 list-all-services animated">
                    <section class="vertical-center slider">
                        <div class="text-slide-vertical">
                            <div class="number-title">01</div>
                            <div>USER INTERFACE & EXPERIENCE</div>
                        </div>
                        <div class="text-slide-vertical">
                            <div class="number-title">02</div>
                            <div>DATA ANALYSIS</div>
                        </div>
                        <div class="text-slide-vertical">
                            <div class="number-title">03</div>
                            <div>FEATURE RESEARCH</div>
                        </div>
                        <div class="text-slide-vertical">
                            <div class="number-title">04</div>
                            <div>E-COMMERCE</div>
                        </div>
                        <div class="text-slide-vertical">
                            <div class="number-title">05</div>
                            <div>WEB HOSTING</div>
                        </div>
                        <div class="text-slide-vertical">
                            <div class="number-title">06</div>
                            <div>DOCUMENTATION</div>
                        </div>
                        <div class="text-slide-vertical">
                            <div class="number-title">07</div>
                            <div>MOBILE APP ENGINEERING</div>
                        </div>
                        <div class="text-slide-vertical">
                            <div class="number-title">08</div>
                            <div>WEB APPLICATION ENGINEERING</div>
                        </div>
                        <div class="text-slide-vertical">
                            <div class="number-title">09</div>
                            <div>DEPLOY & MAINTENANCE</div>
                        </div>
                        <div class="text-slide-vertical">
                            <div class="number-title">10</div>
                            <div>COMPANY PROFILING</div>
                        </div>
                        <div class="text-slide-vertical">
                            <div class="number-title">11</div>
                            <div>SERVER MANAGEMENT</div>
                        </div>
                        <div class="text-slide-vertical">
                            <div class="number-title">12</div>
                            <div>IDEAS & MORE</div>
                        </div>
                    </section>
                </div>
                <div class="col-lg-7">
                    <div class="detail-services animated">
                        <div class="big-title color-primary">
                            <b>12</b> Years of
                            <br>Combined EXpeRIENCE
                        </div>
                        <p class="desc-detail-services">we are certified with UI/UX for agencies award, so you can rest
                            assured we will use every practices possible to make sure you have the best looking, best
                            feeling app ever.</p>
                        <!-- <div class="index-services color-black mt-50 font-bold">01/12</div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section2">
        <div class="row no-gutters h-100 align-items-center responsive1-3dr">
            <div class="col-sm-4 h-100">
                <div class="row align-content-between flex-wrap h-100 text-3dr first-part">
                    <div class="col-12 s3dr-part1 animated">
                        <div class="title-3dr">DEFINE</div>
                        <p>Product Consulting
                            <br>Data Gathering
                            <br>Requirements Analysis
                            <br>UI/UX Design&nbsp;</p>
                    </div>
                    <div class="col-12 s3dr-part4 animated">
                        <div class="title-3dr">REVIEW</div>
                        <p>Feature Iteration Research
                            <br>Refactor Anaylisis
                            <br>Market Focus Group
                            <br>User Experience Evaluation&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 img-3dr animated">
                <div class="d-flex align-items-center justify-content-center">
                    <img class="animated bounceIn" src="{{ url('assets/frontend/img/3dr.svg') }}" style="width: 25rem;">
                </div>
            </div>
            <div class="col-sm-4 h-100">
                <div class="row align-content-between flex-wrap h-100 text-3dr">
                    <div class="col-12 s3dr-part2 animated">
                        <div class="title-3dr">DEVELOP</div>
                        <p>System Architecture Analysis
                            <br>Platform Coding
                            <br>Agile Project Management
                            <br>Rapid Documented Report</p>
                    </div>
                    <div class="col-12 s3dr-part3 animated">
                        <div class="title-3dr">DEPLOY</div>
                        <p>Play / App Store Management
                            <br>Server Deployment
                            <br>Product Launching
                            <br>Product Maintenance</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters align-items-center responsive2-3dr">
            <div class="col-sm-4">
                <div class="row text-3dr">
                    <div class="col-12 s3dr-part1 animated">
                        <h4 class="color-black mb-25">HOW WE WORK (3DR)</h4>
                    </div>
                    <div class="col-12 s3dr-part1 animated">
                        <div class="title-3dr">DEFINE</div>
                        <p>Product Consulting
                            <br>Data Gathering
                            <br>Requirements Analysis
                            <br>UI/UX Design&nbsp;</p>
                    </div>
                    <div class="col-12 s3dr-part2 animated">
                        <div class="title-3dr">REVIEW</div>
                        <p>Feature Iteration Research
                            <br>Refactor Anaylisis
                            <br>Market Focus Group
                            <br>User Experience Evaluation&nbsp;</p>
                    </div>
                    <div class="col-12 s3dr-part3 animated">
                        <div class="title-3dr">DEVELOP</div>
                        <p>System Architecture Analysis
                            <br>Platform Coding
                            <br>Agile Project Management
                            <br>Rapid Documented Report</p>
                    </div>
                    <div class="col-12 s3dr-part4 animated">
                        <div class="title-3dr">DEPLOY</div>
                        <p>Play / App Store Management
                            <br>Server Deployment
                            <br>Product Launching
                            <br>Product Maintenance</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section3">
        <div class="slide">
            <div class="row text-contact justify-content-center">
                <div class="col-sm-4">
                    <object class="img-map-sg animated" data="{{ url('assets/frontend/img/map-sg.svg') }}"
                        type="image/svg+xml"></object>
                </div>
                <div class="col-sm-4 text-map-sg">
                    <div class="title-country animated">singapore</div>
                    <p class="animated">Gemini @ sims
                        <br>(+65) 81579012
                        <br><a class="email-about" href="mailto:sg@maven.co.id?Subject=Hi Maven"
                            target="_top">sg@maven.co.id</a></p>
                </div>
            </div>
            <!-- <div class="img-map">
                    <img src="img/map-sg.png">
                </div> -->
        </div>
        <div class="slide active">
            <div class="row text-contact">
                <div class="col-sm-4 offset-sm-1 text-map-id">
                    <div class="title-country animated">Jakarta</div>
                    <p class="animated">Taman Ratu
                        <br>(+62) 21 5651189
                        <br><a class="email-about" href="mailto:hi@maven.co.id?Subject=Hi Maven"
                            target="_top">hi@maven.co.id</a></p>
                </div>
            </div>
            <div class="img-map">
                <object class="img-map-id animated" data="{{ url('assets/frontend/img/map-id.svg') }}"
                    type="image/svg+xml"></object>
            </div>
        </div>
    </div>
</div>
@stop