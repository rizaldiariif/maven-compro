@extends('layouts.frontend')

@push('styles')
<link rel="stylesheet" href="{{ url('assets/frontend/css/custom.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="{{ url('assets/frontend/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/frontend/js/works.js') }}"></script>
@endpush

@section('content')
<div id="fullpage">
    <div class="section " id="section0">
        <div class="container-custom">
            <div class="static-height-div">
                <div class="typing-title-container animated">
                    <span class="typing-title" id="typing-works">ACTION <br class="title-new-line">SPEAKS
                        <br>LOUDEST</span>
                </div>
                <p class="animation-in animated">we believe that our previous works and records
                    <br>proves us best on what you can expect. it also is
                    <br>something that we take our highest prides in.</p>
            </div>
        </div>
        <div class="number-home animated">03</div>
    </div>
    <div class="section" id="section1">
        <div id="content" class="clients-running-list">
            <object class="img-client-all" id="clients-svg" data="{{ url('assets/frontend/img/clients3.svg') }}"
                type="image/svg+xml"></object>
        </div>
        <div class="client-section d-flex justify-content-center">
            <div class="row justify-content-center no-gutters">
                <div class="col-12">
                    <h1>30+</h1>
                    <div class="text-client">BRANDS</div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section2">
        <img src="{{ url('assets/frontend/img/bg-client.svg') }}" class="bg-circle-client responsive1-3dr">
        <div class="round-section responsive1-3dr">
            <div class="curve"></div>
            <ul>
                <li>.</li>
                <li><a class="active" href="{{ url('works#client1') }}">01</a></li>
                <li><a href="{{ url('works#client2') }}">02</a></li>
                <li><a href="{{ url('works#client3') }}">03</a></li>
                <li><a href="{{ url('works#client4') }}">04</a></li>
                <li><a href="{{ url('works#client5') }}">05</a></li>
                <li><a href="{{ url('works#client6') }}">06</a></li>
                <li><a href="{{ url('works#client7') }}">07</a></li>
                <li><a href="{{ url('works#client8') }}">08</a></li>
            </ul>
        </div>
        <img src="{{ url('assets/frontend/img/works/mummys.png') }}" class="client-detail-img first">
        <div class="container-custom works-section">
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{ url('assets/frontend/img/works/logo-mummys.png') }}">
                    <div class="color-black">
                        <h4>SMART MOM'S<br>COMMERCE</h4>
                        <p>Developing a marketplace of mom’s products based on Singapore needs a deep understanding on
                            how the trend works there. We validated the whole product into several phases to accommodate
                            market needs, delivering amazing mobile & web experience.</p>
                        <div class="category-wroks">android - ios - web
                            <br>design - strategy</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section3">
        <img src="{{ url('assets/frontend/img/bg-client.svg') }}" class="bg-circle-client responsive1-3dr">
        <div class="round-section responsive1-3dr">
            <div class="curve"></div>
            <ul>
                <li>.</li>
                <li><a href="{{ url('works#client1') }}">01</a></li>
                <li><a class="active" href="{{ url('works#client2') }}">02</a></li>
                <li><a href="{{ url('works#client3') }}">03</a></li>
                <li><a href="{{ url('works#client4') }}">04</a></li>
                <li><a href="{{ url('works#client5') }}">05</a></li>
                <li><a href="{{ url('works#client6') }}">06</a></li>
                <li><a href="{{ url('works#client7') }}">07</a></li>
                <li><a href="{{ url('works#client8') }}">08</a></li>
            </ul>
        </div>
        <img src="{{ url('assets/frontend/img/works/polda.png') }}" class="client-detail-img main">
        <div class="container-custom works-section">
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{ url('assets/frontend/img/works/logo-polda.png') }}">
                    <div class="color-black">
                        <h4>INTUITIVE INTUITIVE<br>PANIC BUTTON<br>SOLUTION</h4>
                        <p>Interesting to define a product through work with head of common crimes in Polda Metro Jaya.
                            The vision to give a quick and accurate solution of emergencies, we crafted an app where
                            people can use in distress situation, complemented with an Operator backend app.</p>
                        <div class="category-wroks">android - web
                            <br>design - strategy</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section4">
        <img src="{{ url('assets/frontend/img/bg-client.svg') }}" class="bg-circle-client responsive1-3dr">
        <div class="round-section responsive1-3dr">
            <div class="curve"></div>
            <ul>
                <li>.</li>
                <li><a href="{{ url('works#client1') }}">01</a></li>
                <li><a href="{{ url('works#client2') }}">02</a></li>
                <li><a class="active" href="{{ url('works#client3') }}">03</a></li>
                <li><a href="{{ url('works#client4') }}">04</a></li>
                <li><a href="{{ url('works#client5') }}">05</a></li>
                <li><a href="{{ url('works#client6') }}">06</a></li>
                <li><a href="{{ url('works#client7') }}">07</a></li>
                <li><a href="{{ url('works#client8') }}">08</a></li>
            </ul>
        </div>
        <img src="{{ url('assets/frontend/img/works/fleet.png') }}" class="client-detail-img main">
        <div class="container-custom works-section">
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{ url('assets/frontend/img/works/logo-fleet.png') }}">
                    <div class="color-black">
                        <h4>GO FOOD<br>LIKE<br>DRIVER APP</h4>
                        <p>Implementing a Go-Food like driver app in collaboration with Foodgasm. We directly handle
                            challenges like calculating price over distance, and create a great experience where drivers
                            can accept orders and manage all of his/her earnings in the application.</p>
                        <div class="category-wroks">android - web
                            <br>design - strategy</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section5">
        <img src="{{ url('assets/frontend/img/bg-client.svg') }}" class="bg-circle-client responsive1-3dr">
        <div class="round-section responsive1-3dr">
            <div class="curve"></div>
            <ul>
                <li>.</li>
                <li><a href="{{ url('works#client1') }}">01</a></li>
                <li><a href="{{ url('works#client2') }}">02</a></li>
                <li><a href="{{ url('works#client3') }}">03</a></li>
                <li><a class="active" href="{{ url('works#client4') }}">04</a></li>
                <li><a href="{{ url('works#client5') }}">05</a></li>
                <li><a href="{{ url('works#client6') }}">06</a></li>
                <li><a href="{{ url('works#client7') }}">07</a></li>
                <li><a href="{{ url('works#client8') }}">08</a></li>
            </ul>
        </div>
        <img src="{{ url('assets/frontend/img/works/funding.png') }}" class="client-detail-img main">
        <div class="container-custom works-section">
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{ url('assets/frontend/img/works/logo-funding.png') }}">
                    <div class="color-black">
                        <h4>P2P LENDING<br>FINTECH</h4>
                        <p>Digital disruption in the form of loaning service for businesses also known as p2p lending,
                            is starting to grow rapidly this day. Driven from the high competition on thi svertical.
                            Funding Societies operates in South East Asia reached out to us in a need to deliver an
                            unforgettable experience on delivering this service to their customer by giving them a
                            flawless customer journey from the websiteto excellent service and information provided.</p>
                        <div class="category-wroks">web
                            <br>design - strategy</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section6">
        <img src="{{ url('assets/frontend/img/bg-client.svg') }}" class="bg-circle-client responsive1-3dr">
        <div class="round-section responsive1-3dr">
            <div class="curve"></div>
            <ul>
                <li>.</li>
                <li><a href="{{ url('works#client1') }}">01</a></li>
                <li><a href="{{ url('works#client2') }}">02</a></li>
                <li><a href="{{ url('works#client3') }}">03</a></li>
                <li><a href="{{ url('works#client4') }}">04</a></li>
                <li><a class="active" href="{{ url('works#client5') }}">05</a></li>
                <li><a href="{{ url('works#client6') }}">06</a></li>
                <li><a href="{{ url('works#client7') }}">07</a></li>
                <li><a href="{{ url('works#client8') }}">08</a></li>
            </ul>
        </div>
        <img src="{{ url('assets/frontend/img/works/welbi.png') }}" class="client-detail-img main">
        <div class="container-custom works-section">
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{ url('assets/frontend/img/works/logo-welbi.png') }}">
                    <div class="color-black">
                        <h4>CREDIT BASED<br>PAYMENT SOLUTION</h4>
                        <p>Fintech based app approaches a nice balance of great usability and great security, we
                            collaborated with a foreign development team to work on the product meticulously. Resulting
                            in great MVP to deploy and initiate as a loan based.</p>
                        <div class="category-wroks">android - web
                            <br>design - strategy</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section7">
        <img src="{{ url('assets/frontend/img/bg-client.svg') }}" class="bg-circle-client responsive1-3dr">
        <div class="round-section responsive1-3dr">
            <div class="curve"></div>
            <ul>
                <li>.</li>
                <!-- <li><a href="{{ url('works#client1') }}">01</a></li>
                <li><a href="{{ url('works#client2') }}">02</a></li> -->
                <li><a href="{{ url('works#client3') }}">03</a></li>
                <li><a href="{{ url('works#client4') }}">04</a></li>
                <li><a href="{{ url('works#client5') }}">05</a></li>
                <li><a class="active" href="{{ url('works#client6') }}">06</a></li>
                <li><a href="{{ url('works#client7') }}">07</a></li>
                <li><a href="{{ url('works#client8') }}">08</a></li>
            </ul>
        </div>
        <img src="{{ url('assets/frontend/img/works/usee.png') }}" class="client-detail-img main">
        <div class="container-custom works-section">
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{ url('assets/frontend/img/works/logo-usee.png') }}">
                    <div class="color-black">
                        <h4>DESTINATION MEDIA<br>INDONESIA TRAVEL</h4>
                        <p>In line with Telkom’s target to distribute Indonesia’s local destination. We developed an
                            Android TV Solution where viewers can view local destinations right on their TV-BOX by
                            Telkom. Browsing destination media, see reviews, watch videos, and activities.</p>
                        <div class="category-wroks">android - web
                            <br>design - strategy</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section8">
        <img src="{{ url('assets/frontend/img/bg-client.svg') }}" class="bg-circle-client responsive1-3dr">
        <div class="round-section responsive1-3dr">
            <div class="curve"></div>
            <ul>
                <li>.</li>
                <!-- <li><a href="{{ url('works#client1') }}">01</a></li>
                <li><a href="{{ url('works#client2') }}">02</a></li> -->
                <li><a href="{{ url('works#client3') }}">03</a></li>
                <li><a href="{{ url('works#client4') }}">04</a></li>
                <li><a href="{{ url('works#client5') }}">05</a></li>
                <li><a href="{{ url('works#client6') }}">06</a></li>
                <li><a class="active" href="{{ url('works#client7') }}">07</a></li>
                <li><a href="{{ url('works#client8') }}">08</a></li>
            </ul>
        </div>
        <img src="{{ url('assets/frontend/img/works/love-life.png') }}" class="client-detail-img main">
        <div class="container-custom works-section">
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{ url('assets/frontend/img/works/logo-love-life.png') }}">
                    <div class="color-black">
                        <h4>LIFE INSURANCE<br>E-COMMERCE</h4>
                        <p>Life insurance is always known with his conventional approach to reach their customer. End up
                            with this stigma, we build up a new digital presence that deliver great experience also
                            insightful web pages. The goal is to help people to get an easy and informative page about
                            insurance product that suits for them.</p>
                        <div class="category-wroks">web
                            <br>design - strategy</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section9">
        <img src="{{ url('assets/frontend/img/bg-client.svg') }}" class="bg-circle-client responsive1-3dr">
        <div class="round-section responsive1-3dr">
            <div class="curve"></div>
            <ul>
                <li>.</li>
                <!-- <li><a href="{{ url('works#client1') }}">01</a></li>
                <li><a href="{{ url('works#client2') }}">02</a></li> -->
                <li><a href="{{ url('works#client3') }}">03</a></li>
                <li><a href="{{ url('works#client4') }}">04</a></li>
                <li><a href="{{ url('works#client5') }}">05</a></li>
                <li><a href="{{ url('works#client6') }}">06</a></li>
                <li><a href="{{ url('works#client7') }}">07</a></li>
                <li><a class="active" href="{{ url('works#client8') }}">08</a></li>
            </ul>
        </div>
        <img src="{{ url('assets/frontend/img/works/one21.png') }}" class="client-detail-img main">
        <div class="container-custom works-section">
            <div class="row">
                <div class="col-12 col-md-6">
                    <img src="{{ url('assets/frontend/img/works/logo-one21.png') }}">
                    <div class="color-black">
                        <h4>SALES AGENT<br>SOCIAL MEDIA</h4>
                        <p>There are a lot of sales dashboard software and solution, but rarely a project sounds like a
                            social engagement for the sales agent teams. Our collaboration with a partner agency
                            develops an internal social media to be used by sales agents in the company to engage.</p>
                        <div class="category-wroks">android - web
                            <br>design - strategy</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section10">
        <div class="slide">
            <div class="row text-contact justify-content-center">
                <div class="col-sm-4">
                    <object class="img-map-sg animated" data="{{ url('assets/frontend/img/map-sg.svg') }}"
                        type="image/svg+xml"></object>
                </div>
                <div class="col-sm-4 text-map-sg">
                    <div class="title-country animated">singapore</div>
                    <p class="animated">Gemini @ sims
                        <br>(+65) 81579012
                        <br><a class="email-about" href="mailto:sg@maven.co.id?Subject=Hi Maven"
                            target="_top">sg@maven.co.id</a></p>
                </div>
            </div>
            <!-- <div class="img-map">
                    <img src="img/map-sg.png">
                </div> -->
        </div>
        <div class="slide active">
            <div class="row text-contact">
                <div class="col-sm-4 offset-sm-1 text-map-id">
                    <div class="title-country animated">Jakarta</div>
                    <p class="animated">Taman Ratu
                        <br>(+62) 21 5651189
                        <br><a class="email-about" href="mailto:hi@maven.co.id?Subject=Hi Maven"
                            target="_top">hi@maven.co.id</a></p>
                </div>
            </div>
            <div class="img-map">
                <object class="img-map-id animated" data="{{ url('assets/frontend/img/map-id.svg') }}"
                    type="image/svg+xml"></object>
            </div>
        </div>
    </div>
</div>
@stop