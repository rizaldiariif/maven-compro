@extends('layouts.frontend')

@push('styles')
<link rel="stylesheet" href="{{ url('assets/frontend/css/custom.css') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="{{ url('assets/frontend/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/frontend/js/about.js') }}"></script>
@endpush

@section('content')
<div id="fullpage">
    <div class="section " id="section0">
        <div class="container-custom">
            <div class="static-height-div">
                <div class="typing-title-container animated">
                    <span class="typing-title" id="typing-about">GET TO KNOW <br>MAVEN</span>
                </div>
                <p class="animation-in animated">we operate in Indonesia and Singapore to be the
                    <br>central reach of growing and established market.
                    <br>Our capabilities is in the strenght of teams.</p>
            </div>
        </div>
        <div class="number-home animated">04</div>
    </div>
    <div class="section" id="section1">
        <div class="vision-section main-page-about d-flex justify-content-center">
            <div class="row no-gutters">
                <div class="col-12">
                    <h1 class="visi-h1 animated">VISION</h1>
                    <div class="text-vision animated font-black mb-0">Delivering world-class ROI
                        <br>Driven Digital Products
                        <br>solving real business
                        <br>challenges through Data
                        <br>Driven Technology</div>
                </div>
            </div>
        </div>
        <div class="container-custom d-flex justify-content-center">
            <div class="about-us main-page-about">
                <div class="bg-shadow-about"></div>
                <div class="about-side-btn animated">
                    <label>
                        <input class="side-desc-checkbox" type='checkbox'>
                        <div class='menu see-me animated'>
                            <span class='text-click arrow'><i class="fas fa-chevron-left"></i></span><span
                                class='text-click mission-side-text'>Mission</span>
                        </div>
                        <div class="side-desc">
                            <h1 class="misi-h1 color-white mb-40 animated">MISSION</h1>
                            <p class="mb-0">Be the most impactful
                                <br>Digital Innovation
                                <br>Partner for enterprise
                                <br>businesses across Asia
                            </p>
                        </div>
                    </label>
                    <div class="arrow-minimize">
                        <a class="close-side-desc" href="#">
                            <i class="fas fa-chevron-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section2">
        <div class="slide">
            <div class="row text-contact justify-content-center">
                <div class="col-sm-4">
                    <object class="img-map-sg animated" data="{{ url('assets/frontend/img/map-sg.svg') }}"
                        type="image/svg+xml"></object>
                </div>
                <div class="col-sm-4 text-map-sg">
                    <div class="title-country animated">singapore</div>
                    <p class="animated">Gemini @ sims
                        <br>(+65) 81579012
                        <br><a class="email-about" href="mailto:sg@maven.co.id?Subject=Hi Maven"
                            target="_top">sg@maven.co.id</a></p>
                </div>
            </div>
            <!-- <div class="img-map">
                    <img src="img/map-sg.png">
                </div> -->
        </div>
        <div class="slide active">
            <div class="row text-contact">
                <div class="col-sm-4 offset-sm-1 text-map-id">
                    <div class="title-country animated">Jakarta</div>
                    <p class="animated">Taman Ratu
                        <br>(+62) 21 5651189
                        <br><a class="email-about" href="mailto:hi@maven.co.id?Subject=Hi Maven"
                            target="_top">hi@maven.co.id</a></p>
                </div>
            </div>
            <div class="img-map">
                <object class="img-map-id animated" data="{{ url('assets/frontend/img/map-id.svg') }}"
                    type="image/svg+xml"></object>
            </div>
        </div>
    </div>
</div>
@stop