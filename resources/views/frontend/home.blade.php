@extends('layouts.frontend')

@push('styles')
<link rel="preload" as="style" href="{{ url('assets/frontend/plugins/owl-carousel/owl.carousel.min.css') }}">
<link rel="preload" as="style" href="{{ url('assets/frontend/plugins/owl-carousel/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ url('assets/frontend/css/custom.css?version=2') }}">
@endpush

@push('scripts')
<script type="text/javascript" src="{{ url('assets/frontend/plugins/owl-carousel/owl.carousel.min.js') }}" defer>
</script>
<script type="text/javascript" src="{{ url('assets/frontend/js/custom.js?version=2') }}" defer></script>
<script type="text/javascript" src="{{ url('assets/frontend/js/home.js') }}" defer></script>
@endpush

@section('content')
<div id="fullpage">
    <div class="section" id="section0">
        <div class="container-custom">
            <!-- <h1 class="h1-home">LIMITLESS<br>DIGITAL INNOVATION</h1> -->
            <div class="static-height-div">
                <div class="typing-title-container animated">
                    <span class="typing-title" id="typing-home">DIGITAL <br><b>ENABLER</b></span>
                </div>
                <p class="animation-in animated">We transform businesses across asia to win the market <br>through ROI
                    driven Digital Products, you should only<br><b>hire the best and let's talk your needs.</b></p>
                <a href="{{url('works')}}" class="main-button">OUR WORKS</a>
                <img src="{{url('assets/frontend/img/home-device.png')}}" alt="" class="home-device">
            </div>
        </div>
        <div class="number-home animated" style="z-index: -1">01</div>
    </div>
    <div class="section" id="section1">
        <div class="about-title animated">
            <span>ABOUT<br>US</span>
        </div>
        <div class="container-custom d-flex justify-content-center">
            <div class="about-us">
                <div class="bg-shadow-about"></div>
                <div class="row margin-about-home">
                    <div class="col-12 about-count1 animated">
                        <div class="title-about first"><span class="count1">176</span>+</div>
                        <p>IMPACTED PROJECTS</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 about-count2 animated">
                        <div class="title-about"><span class="count2">30</span>+</div>
                        <p>BRANDS</p>
                    </div>
                    <div class="col-md-4 about-count3 animated">
                        <div class="title-about"><span class="count3">5</span></div>
                        <p>COUNTRIES</p>
                    </div>
                    <div class="col-md-4 about-count4 animated">
                        <div class="title-about"><span class="count4">32</span>+</div>
                        <p>TEAM MEMBERS</p>
                    </div>
                </div>
                <div class="about-side-btn animated">
                    <label>
                        <input class="side-desc-checkbox" type='checkbox'>
                        <div class='menu see-me animated'>
                            <span class='text-click arrow'><i class="fas fa-chevron-left"></i></span><span
                                class='text-click'>See Me</span>
                        </div>
                        <div class="side-desc">
                            <p>
                                "if you think it's
                                <br> expensive to hire a pro to do the job, wait until you hire an amateur."
                                <br>
                                <br>Red Adair, American Oil Well Firefighter (1915 - 2004)
                            </p>
                        </div>
                    </label>
                    <div class="arrow-minimize">
                        <a class="close-side-desc" href="#">
                            <i class="fas fa-chevron-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section2">
        <div class="bg-wrap1 animated">
            <div class="bg-slide-services1 animated"></div>
        </div>
        <!-- <div class="bg-slide-services"></div> -->
        <div class="container-fluid h-100 slide-vertical-services">
            <div class="row justify-content-center align-items-center h-100">
                <div class="col-lg-7">
                    <div class="section-title-services animated">
                        <div class="services-title animated">world class</div>
                        <div class="services-subtitle animated">services</div>
                        <div class="services-desc-3 animated">
                            <p class="color-black font-18 font-normal mt-15 mb-20">ROI driven Digital Products. only by
                                crafting<br>through amazing well thought out disclipines, see<br>below to get a feel of
                                what we can help you with.</p>
                            <a class="color-primary" href="{{ url('services') }}">SEE MORE</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <section class="vertical-center-3 slider animated">
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="1" href="{{ url('services#offered-services') }}">
                                <h3>01</h3>
                                <div class="title-service">USER INTERFACE & EXPERIENCE</div>
                            </a>
                        </div>
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="2" href="{{ url('services#offered-services') }}">
                                <h3>02</h3>
                                <div class="title-service">DATA ANALYSIS</div>
                            </a>
                        </div>
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="3" href="{{ url('services#offered-services') }}">
                                <h3>03</h3>
                                <div class="title-service">FEATURE RESEARCH</div>
                            </a>
                        </div>
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="4" href="{{ url('services#offered-services') }}">
                                <h3>04</h3>
                                <div class="title-service">E-COMMERCE</div>
                            </a>
                        </div>
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="5" href="{{ url('services#offered-services') }}">
                                <h3>05</h3>
                                <div class="title-service">WEB HOSTING</div>
                            </a>
                        </div>
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="6" href="{{ url('services#offered-services') }}">
                                <h3>06</h3>
                                <div class="title-service">DOCUMENTATION</div>
                            </a>
                        </div>
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="7" href="{{ url('services#offered-services') }}">
                                <h3>07</h3>
                                <div class="title-service">MOBILE APP ENGINEERING</div>
                            </a>
                        </div>
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="8" href="{{ url('services#offered-services') }}">
                                <h3>08</h3>
                                <div class="title-service">WEB APPLICATION ENGINEERING</div>
                            </a>
                        </div>
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="9" href="{{ url('services#offered-services') }}">
                                <h3>09</h3>
                                <div class="title-service">DEPLOY & MAINTENANCE</div>
                            </a>
                        </div>
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="10" href="{{ url('services#offered-services') }}">
                                <h3>10</h3>
                                <div class="title-service">COMPANY PROFILING</div>
                            </a>
                        </div>
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="11" href="{{ url('services#offered-services') }}">
                                <h3>11</h3>
                                <div class="title-service">SERVER MANAGEMENT</div>
                            </a>
                        </div>
                        <div class="text-slide-vertical">
                            <a class="services-click" data-id="12" href="{{ url('services#offered-services') }}">
                                <h3>12</h3>
                                <div class="title-service">IDEAS & MORE</div>
                            </a>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <div class="section works-section-home" id="section3">
        <div class="row no-gutters h-100">
            <div class="col-12 col-md-4 box-porto1">
                <div class="bg-porto">
                    <div class="row h-100">
                        <div class="col-12 align-self-center ">
                            <div class="content-porto ">
                                <div class="number">01</div>
                                <div class="title-porto">Mummys<br>Marketplace
                                    <div><a href="{{ url('works#client1') }}" class="link-porto">see detail</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="img-porto-hover mummys"><img
                                src="{{ url('assets/frontend/img/porto-mummys.png') }}"></div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 box-porto2">
                <div class="bg-porto">
                    <div class="row h-100">
                        <div class="col-12 align-self-center ">
                            <div class="content-porto ">
                                <div class="number">02</div>
                                <div class="title-porto">Astra
                                    <br>Life<div><a href="{{ url('works#client7') }}" class="link-porto">see detail</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="img-porto-hover astra"><img src="{{ url('assets/frontend/img/porto-astra.png') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 box-porto3">
                <div class="bg-porto">
                    <div class="row h-100">
                        <div class="col-12 align-self-center ">
                            <div class="content-porto ">
                                <div class="number">03</div>
                                <div class="title-porto">Polres
                                    <br>Karawang<div><a href="{{ url('works#client2') }}" class="link-porto">see
                                            detail</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="img-porto-hover polda"><img src="{{ url('assets/frontend/img/porto-polda.png') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section4">
        <div class="slide">
            <div class="row text-contact justify-content-center">
                <div class="col-sm-4">
                    <object class="img-map-sg animated" data="{{ url('assets/frontend/img/map-sg.svg') }}"
                        type="image/svg+xml"></object>
                </div>
                <div class="col-sm-4 text-map-sg">
                    <div class="title-country animated">singapore</div>
                    <p class="animated">Gemini @ sims
                        <br>(+65) 81579012
                        <br><a class="email-about" href="mailto:sg@maven.co.id?Subject=Hi Maven"
                            target="_top">sg@maven.co.id</a></p>
                </div>
            </div>
            <!-- <div class="img-map">
                    <img src="img/map-sg.png">
                </div> -->
        </div>
        <div class="slide active">
            <div class="row text-contact">
                <div class="col-sm-4 offset-sm-1 text-map-id">
                    <div class="title-country animated">Jakarta</div>
                    <p class="animated">Taman Ratu
                        <br>(+62) 21 5651189
                        <br><a class="email-about" href="mailto:hi@maven.co.id?Subject=Hi Maven"
                            target="_top">hi@maven.co.id</a></p>
                </div>
            </div>
            <div class="img-map">
                <object class="img-map-id animated" data="{{ url('assets/frontend/img/map-id.svg') }}"
                    type="image/svg+xml"></object>
            </div>
        </div>
    </div>
</div>
@stop