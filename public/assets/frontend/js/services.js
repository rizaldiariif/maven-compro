$(document).ready(function () {
	$('#fullpage').fullpage({
		anchors: ['index', 'offered-services', '3dr', 'contact'],
		sectionsColor: ['#2B3885', '#fff', '#fff', '#2B3885'],
		css3: true,
		scrollingSpeed: 800,
		onLeave: function (index, nextIndex, direction) {
			var leavingSection = $(this);
			if (index == 1 && direction == 'down') {
				$('.logo-maven').fadeOut();
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
				$('.animation-in').removeClass('fadeInUp').addClass('fadeOutLeft');
				$('.typing-title-container').removeClass('fadeInUp').addClass('fadeOutLeft');
				$('.number-home').removeClass('fadeInRight').addClass('fadeOutRight');
				$('.bg-slide-services2').removeClass('fadeOutLeft').addClass('slideInLeft');
				$('.list-all-services').removeClass('fadeOutLeft').addClass('slideInLeft');
				$('.detail-services').removeClass('zoomOut').addClass('zoomIn');
				$('.scroll-icon').css('display', 'none');
			} else if (index == 2 && direction == 'up') {
				$('.logo-maven').fadeIn().css('display', 'initial');
				$('#header').removeClass('page2');
				$('.open span').css('background-color', '#fff');
				setTimeout(
					function () {
						$('.typing-title-container').removeClass('fadeOutLeft').addClass('fadeInUp');
					}, 400);
				setTimeout(
					function () {
						$('.animation-in').removeClass('fadeOutLeft').addClass('fadeInUp');
					}, 700);
				setTimeout(
					function () {
						$('.number-home').removeClass('fadeOutRight').addClass('fadeInRight');
					}, 500);
				$('.scroll-icon').css('display', 'inherit');
				$('#header').removeClass('header-services');
				$('.bg-slide-services2').removeClass('slideInLeft').addClass('fadeOutLeft');
				$('.list-all-services').removeClass('slideInLeft').addClass('fadeOutLeft');
				$('.detail-services').removeClass('zoomIn').addClass('zoomOut');
			} else if (index == 2 && direction == 'down') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
				$('#header').removeClass('header-services');
				$('.bg-slide-services2').removeClass('slideInLeft').addClass('fadeOutLeft');
				$('.list-all-services').removeClass('slideInLeft').addClass('fadeOutLeft');
				$('.detail-services').removeClass('zoomIn').addClass('zoomOut');
				$('.img-3dr').removeClass('zoomOut').addClass('zoomIn');
				setTimeout(
					function () {
						$('.s3dr-part1').css('opacity', '1').removeClass('zoomOut').addClass('zoomIn');
					}, 300);
				setTimeout(
					function () {
						$('.s3dr-part2').css('opacity', '1').removeClass('zoomOut').addClass('zoomIn');
					}, 500);
				setTimeout(
					function () {
						$('.s3dr-part3').css('opacity', '1').removeClass('zoomOut').addClass('zoomIn');
					}, 700);
				setTimeout(
					function () {
						$('.s3dr-part4').css('opacity', '1').removeClass('zoomOut').addClass('zoomIn');
					}, 900);
			} else if (index == 3 && direction == 'up') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
				$('.bg-slide-services2').removeClass('fadeOutLeft').addClass('slideInLeft');
				$('.list-all-services').removeClass('fadeOutLeft').addClass('slideInLeft');
				$('.detail-services').removeClass('zoomOut').addClass('zoomIn');
				$('.img-3dr').removeClass('zoomIn').addClass('zoomOut');
				$('.s3dr-part1').removeClass('zoomIn').addClass('zoomOut').css('opacity', '0');
				$('.s3dr-part2').removeClass('zoomIn').addClass('zoomOut').css('opacity', '0');
				$('.s3dr-part3').removeClass('zoomIn').addClass('zoomOut').css('opacity', '0');
				$('.s3dr-part4').removeClass('zoomIn').addClass('zoomOut').css('opacity', '0');
			} else if (index == 3 && direction == 'down') {
				$('#header').removeClass('page2');
				$('.open span').css('background-color', '#fff');
				$('.img-3dr').removeClass('zoomIn').addClass('zoomOut');
				$('.s3dr-part1').removeClass('zoomIn').addClass('zoomOut').css('opacity', '0');
				$('.s3dr-part2').removeClass('zoomIn').addClass('zoomOut').css('opacity', '0');
				$('.s3dr-part3').removeClass('zoomIn').addClass('zoomOut').css('opacity', '0');
				$('.s3dr-part4').removeClass('zoomIn').addClass('zoomOut').css('opacity', '0');
				$('.text-map-sg .title-country').removeClass('fadeOut').addClass('fadeInUp');
				$('.text-map-id .title-country').removeClass('fadeOut').addClass('fadeInUp');
				$('.text-map-sg p').removeClass('fadeOut').addClass('fadeInUp');
				$('.text-map-id p').removeClass('fadeOut').addClass('fadeInUp');
			} else if (index == 4 && direction == 'up') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
				$('.text-map-sg .title-country').removeClass('fadeInUp').addClass('fadeOut');
				$('.text-map-id .title-country').removeClass('fadeInUp').addClass('fadeOut');
				$('.text-map-sg p').removeClass('fadeInUp').addClass('fadeOut');
				$('.text-map-id p').removeClass('fadeInUp').addClass('fadeOut');
				$('.img-3dr').removeClass('zoomOut').addClass('zoomIn');
				setTimeout(
					function () {
						$('.s3dr-part1').css('opacity', '1').removeClass('zoomOut').addClass('zoomIn');
					}, 300);
				setTimeout(
					function () {
						$('.s3dr-part2').css('opacity', '1').removeClass('zoomOut').addClass('zoomIn');
					}, 500);
				setTimeout(
					function () {
						$('.s3dr-part3').css('opacity', '1').removeClass('zoomOut').addClass('zoomIn');
					}, 700);
				setTimeout(
					function () {
						$('.s3dr-part4').css('opacity', '1').removeClass('zoomOut').addClass('zoomIn');
					}, 900);
			}
		},
		afterLoad: function () {
			var url_page = $(location).attr('href');
			var menu_name = ['index', 'offered-services', '3dr', 'contact'];
			if (url_page.indexOf(menu_name[0]) != -1) {
				$('.logo-maven').fadeIn().css('display', 'initial');
				$('#header').removeClass('page2');
				$('.open span').css('background-color', '#fff');
			} else if (url_page.indexOf(menu_name[1]) != -1) {
				$('.logo-maven').fadeOut();
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
				$('#header').addClass('header-services');
			} else if (url_page.indexOf(menu_name[2]) != -1) {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (url_page.indexOf(menu_name[3]) != -1) {
				$('#header').removeClass('page2');
				$('.open span').css('background-color', '#fff');
				var hash_url = $(location).attr('hash');
				var slide_id = "#contact/1";
				var slide_sg = "#contact";
				if (hash_url === slide_id) {
					$('.fp-controlArrow.fp-next').css('display', 'none');
					$('.fp-controlArrow.fp-prev').css('display', 'inherit');
				} else if (hash_url === slide_sg) {
					$('.fp-controlArrow.fp-next').css('display', 'inherit');
					$('.fp-controlArrow.fp-prev').css('display', 'none');
				}
			}
		},
		afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
			if (index == 4) {
				$('.fp-controlArrow.fp-next').css('display', 'inherit');
				$('.fp-controlArrow.fp-prev').css('display', 'none');

			}
			if (slideIndex == 1) {
				$('.fp-controlArrow.fp-next').css('display', 'none');
				$('.fp-controlArrow.fp-prev').css('display', 'inherit');

			}
		},
		onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) {
			if (index == 4 && direction == 'right') {
				$('.fp-controlArrow.fp-next').css('display', 'none');
				setTimeout(
					function () {
						$('.text-map-id .title-country').removeClass('fadeOutLeft').addClass('fadeInUp');
						// $('.img-map-id').removeClass('fadeOutRight').addClass('fadeInRight');
						// out sg
						$('.text-map-sg .title-country').removeClass('fadeInUp').addClass('fadeOutRight');
						// $('.img-map-sg').removeClass('fadeInLeft').addClass('fadeOutLeft');
						$('.text-map-sg p').removeClass('fadeInUp').addClass('fadeOutRight');
					}, 0);
				setTimeout(
					function () {
						$('.text-map-id p').removeClass('fadeOutLeft').addClass('fadeInUp');
					}, 100);
			}
			if (slideIndex == 1 && direction == 'left') {
				$('.fp-controlArrow.fp-prev').css('display', 'none');
				setTimeout(
					function () {
						$('.text-map-sg .title-country').removeClass('fadeOutRight').addClass('fadeInUp');
						// $('.img-map-sg').removeClass('fadeOutLeft').addClass('fadeInLeft');
						// out id
						$('.text-map-id .title-country').removeClass('fadeInUp').addClass('fadeOutLeft');
						// $('.img-map-id').removeClass('fadeInRight').addClass('fadeOutRight');
						$('.text-map-id p').removeClass('fadeInUp').addClass('fadeOutLeft');
					}, 0);
				setTimeout(
					function () {
						$('.text-map-sg p').removeClass('fadeOutRight').addClass('fadeInUp');
					}, 100);
			}
		}
	});
});

$(document).ready(function () {
	$('#section3 .fp-prev').append('<i class="fas fa-chevron-left"></i>');
	$('#section3 .fp-next').append('<i class="fas fa-chevron-right"></i>');
});

$('#vertical-prev').click(function () {
	$(".vertical-center").slick('slickPrev');
});

$('#vertical-next').click(function () {
	$(".vertical-center").slick('slickNext');
});

var get_id = null;

if ($.session.get("slide-id") == null) {
	get_id = 0;
} else {
	get_id = parseInt($.session.get("slide-id")) - 1;
}

$(document).on('ready', function () {
	$(".vertical-center").slick({
		dots: false,
		arrows: false,
		verticalSwiping: false,
		vertical: true,
		centerMode: true,
		initialSlide: get_id,
		slidesToShow: 3,
		slidesToScroll: 3
	}).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
		if (nextSlide === 0) {
			$('.desc-detail-services').text("we are certified with UI/UX for agencies award, so you can rest assured we will use every practices possible to make sure you have the best looking, best feeling app ever.");
			$('.index-services').text("01/12");
		} else if (nextSlide === 1) {
			$('.desc-detail-services').text("every wondered if your app, heck, your business is doing well in the digital space ? we can analyze the ups the downs of the site/app, all in the name to push future improvements.");
			$('.index-services').text("02/12");
		} else if (nextSlide === 2) {
			$('.desc-detail-services').text("we believe a great digital product doesn't stop after it's first conceiving. It keeps growing, and we are here to help, through methods like focus groups, interview, etc. updates.");
			$('.index-services').text("03/12");
		} else if (nextSlide === 3) {
			$('.desc-detail-services').text("everything works better in digital, sell your products and stock through your website. Features implemented such as shopping cart, payment gateway, and shipping.");
			$('.index-services').text("04/12");
		} else if (nextSlide === 4) {
			$('.desc-detail-services').text("have it up in the world wide web, all the time for everyone to access. we provide the whole thing from domain purchasing, hosting management, and FTP deployment.");
			$('.index-services').text("05/12");
		} else if (nextSlide === 5) {
			$('.desc-detail-services').text("every digital product can be requested to be complemented with well written documents. User manuals for usage, architecture structure, code base, for ease of use.");
			$('.index-services').text("06/12");
		} else if (nextSlide === 6) {
			$('.desc-detail-services').text("we are experts in developing, tests, and deploy with no exception but using the best possible solution, native tech. wether it is Kotlin for Android, or Swift 3 for iOS.");
			$('.index-services').text("07/12");
		} else if (nextSlide === 7) {
			$('.desc-detail-services').text("websites, web application, backend development, you name it. if it opens in your browser, we can make it. it is foremost the easiest accessable digital product.");
			$('.index-services').text("08/12");
		} else if (nextSlide === 8) {
			$('.desc-detail-services').text("we make sure that the app or site is running perfectly, we constantly monitor to make sure the slighest moments is looks like its crashing, you won't even know it.");
			$('.index-services').text("09/12");
		} else if (nextSlide === 9) {
			$('.desc-detail-services').text("definitely the first you should do! we are well versed in digital branding, showing what your company do, values, and one step closer to digital innovation.");
			$('.index-services').text("10/12");
		} else if (nextSlide === 10) {
			$('.desc-detail-services').text("every digital product relies on a great server to accomodate it's growing user. we provide full service from setting up the VPS server, load balancing, and recovery methods.");
			$('.index-services').text("11/12");
		} else if (nextSlide === 11) {
			$('.desc-detail-services').text("this is underated, we believe our experience in multinational brands and multidisciplinary industries can give great input, and ideas for your business to bring it to new heights.");
			$('.index-services').text("12/12");
		}
	});
});

$.session.remove('slide-id');

$(document).on('ready', function () {
	$(".center")
		.on('afterChange init', function (event, slick, direction) {
			// remove all prev/next
			slick.$slides.removeClass('left-slide-current').removeClass('right-slide-current');
			for (var i = 0; i < slick.$slides.length; i++) {
				var $slide = $(slick.$slides[i]);
				if ($slide.hasClass('slick-current')) {
					// update DOM siblings
					$slide.prev().addClass('left-slide-current');
					$slide.next().addClass('right-slide-current');
					break;
				}
			}
		})
		.on('beforeChange', function (event, slick) {
			slick.$slides.removeClass('left-slide-current').removeClass('right-slide-current');
			$(".slick-center").css("transition", "opacity 500ms ease-out");
		})
		.slick({
			dots: false,
			arrows: false,
			infinite: true,
			centerMode: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			centerPadding: '0px',
		});

});


// function typeServices() {
// 	var typed3 = new Typed('#typing-services', {
// 		strings: ['We <br class="title-new-line">CREATE <br>WORLD-CLASS'],
// 		typeSpeed: 25,
// 		loop: false,
// 	});
// }


$(window).load(function () {
	// setTimeout(function () {
	// 	typeServices();
	// }, 2100);
	setTimeout(function () {
		$('.animation-in').css('display', 'block').addClass('fadeInUp');
	}, 2600);

});