$(document).ready(function () {
	$('#fullpage').fullpage({
		anchors: ['index', 'client-list', 'client1', 'client2', 'client3', 'client4', 'client5', 'client6', 'client7', 'client8', 'contact'],
		sectionsColor: ['#2B3885', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#fff', '#2B3885'],
		css3: true,
		scrollingSpeed: 800,
		onLeave: function (index, nextIndex, direction) {
			var leavingSection = $(this);
			if (index == 1 && direction == 'down') {
				$('.logo-maven').fadeOut();
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
				$('.animation-in').removeClass('fadeInUp').addClass('fadeOutLeft');
				$('.typing-title-container').removeClass('fadeInUp').addClass('fadeOutLeft');
				$('.number-home').removeClass('fadeInRight').addClass('fadeOutRight');
				$('.scroll-icon').css('opacity', '0');
			} else if (index == 2 && direction == 'up') {
				$('.logo-maven').fadeIn().css('display', 'initial');
				$('#header').removeClass('page2');
				$('.open span').css('background-color', '#fff');
				setTimeout(
					function () {
						$('.typing-title-container').removeClass('fadeOutLeft').addClass('fadeInUp');
					}, 400);
				setTimeout(
					function () {
						$('.animation-in').removeClass('fadeOutLeft').addClass('fadeInUp');
					}, 700);
				setTimeout(
					function () {
						$('.number-home').removeClass('fadeOutRight').addClass('fadeInRight');
					}, 500);
				$('#header').removeClass('header-works');
				$('.scroll-icon').css('opacity', '1');
			} else if (index == 2 && direction == 'down') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
				$('#header').removeClass('header-works');
				// $.fn.fullpage.setAllowScrolling(false, 'down');
			} else if (index == 3 && direction == 'up') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 3 && direction == 'down') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 4 && direction == 'up') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 4 && direction == 'down') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 5 && direction == 'up') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 5 && direction == 'down') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 6 && direction == 'up') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 6 && direction == 'down') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 7 && direction == 'up') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 7 && direction == 'down') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 8 && direction == 'up') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 8 && direction == 'down') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 9 && direction == 'up') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 9 && direction == 'down') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 10 && direction == 'up') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (index == 10 && direction == 'down') {
				$('#header').removeClass('page2');
				$('.open span').css('background-color', '#fff');
				$('.text-map-sg .title-country').removeClass('fadeOut').addClass('fadeInUp');
				$('.text-map-id .title-country').removeClass('fadeOut').addClass('fadeInUp');
				$('.text-map-sg p').removeClass('fadeOut').addClass('fadeInUp');
				$('.text-map-id p').removeClass('fadeOut').addClass('fadeInUp');
			} else if (index == 11 && direction == 'up') {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
				$('.text-map-sg .title-country').removeClass('fadeInUp').addClass('fadeOut');
				$('.text-map-id .title-country').removeClass('fadeInUp').addClass('fadeOut');
				$('.text-map-sg p').removeClass('fadeInUp').addClass('fadeOut');
				$('.text-map-id p').removeClass('fadeInUp').addClass('fadeOut');
			}
		},
		afterLoad: function () {
			var url_page = $(location).attr('href');
			var menu_name = ['index', 'client-list', 'client1', 'client2', 'client3', 'client4', 'client5', 'client6', 'client7', 'client8', 'contact'];
			if (url_page.indexOf(menu_name[0]) != -1) {
				$('.logo-maven').fadeIn().css('display', 'initial');
				$('#header').removeClass('page2');
				$('.open span').css('background-color', '#fff');
			} else if (url_page.indexOf(menu_name[1]) != -1) {
				$('.logo-maven').fadeOut();
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
				$('#header').addClass('header-works');
			} else if (url_page.indexOf(menu_name[2]) != -1) {
				$('#header').addClass('page2');
				$('.open span').css('background-color', '#000');
			} else if (url_page.indexOf(menu_name[10]) != -1) {
				$('#header').removeClass('page2');
				$('.open span').css('background-color', '#fff');
				var hash_url = $(location).attr('hash');
				var slide_id = "#contact/1";
				var slide_sg = "#contact";
				if (hash_url === slide_id) {
					$('.fp-controlArrow.fp-next').css('display', 'none');
					$('.fp-controlArrow.fp-prev').css('display', 'inherit');
				} else if (hash_url === slide_sg) {
					$('.fp-controlArrow.fp-next').css('display', 'inherit');
					$('.fp-controlArrow.fp-prev').css('display', 'none');
				}
			}
		},
		afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
			if (index == 11) {
				$('.fp-controlArrow.fp-next').css('display', 'inherit');
				$('.fp-controlArrow.fp-prev').css('display', 'none');

			}
			if (slideIndex == 1) {
				$('.fp-controlArrow.fp-next').css('display', 'none');
				$('.fp-controlArrow.fp-prev').css('display', 'inherit');

			}
		},
		onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) {
			if (index == 11 && direction == 'right') {
				$('.fp-controlArrow.fp-next').css('display', 'none');
				setTimeout(
					function () {
						$('.text-map-id .title-country').removeClass('fadeOutLeft').addClass('fadeInUp');
						// $('.img-map-id').removeClass('fadeOutRight').addClass('fadeInRight');
						// out sg
						$('.text-map-sg .title-country').removeClass('fadeInUp').addClass('fadeOutRight');
						// $('.img-map-sg').removeClass('fadeInLeft').addClass('fadeOutLeft');
						$('.text-map-sg p').removeClass('fadeInUp').addClass('fadeOutRight');
					}, 0);
				setTimeout(
					function () {
						$('.text-map-id p').removeClass('fadeOutLeft').addClass('fadeInUp');
					}, 100);
			}
			if (slideIndex == 1 && direction == 'left') {
				$('.fp-controlArrow.fp-prev').css('display', 'none');
				setTimeout(
					function () {
						$('.text-map-sg .title-country').removeClass('fadeOutRight').addClass('fadeInUp');
						// $('.img-map-sg').removeClass('fadeOutLeft').addClass('fadeInLeft');
						// out id
						$('.text-map-id .title-country').removeClass('fadeInUp').addClass('fadeOutLeft');
						// $('.img-map-id').removeClass('fadeInRight').addClass('fadeOutRight');
						$('.text-map-id p').removeClass('fadeInUp').addClass('fadeOutLeft');
					}, 0);
				setTimeout(
					function () {
						$('.text-map-sg p').removeClass('fadeOutRight').addClass('fadeInUp');
					}, 100);
			}
		}
	});
});

$(document).ready(function () {
	$('#section10 .fp-prev').append('<i class="fas fa-chevron-left"></i>');
	$('#section10 .fp-next').append('<i class="fas fa-chevron-right"></i>');
});


// var interval = setInterval(function() { 
// 		// $("#content .img-client-all:first-child").remove();
// 		// $("#content .img-client-all").clone().appendTo("#section1 #content");
// 		console.log('test');
// }, 10000);

$(document).ready(function () {
	var a = document.getElementById("clients-svg");
	a.addEventListener("load", function () {
		var svgDoc = a.contentDocument;
		var svgRoot = svgDoc.documentElement;
		$(svgRoot).on('wheel', function (e) {
			if (e.originalEvent.deltaY < 0) {
				// wheeled up
				// $.fn.fullpage.moveSectionUp();
				$.fn.fullpage.moveTo(1);
			}
			else {
				// wheeled down
				// $.fn.fullpage.moveSectionDown();
				$.fn.fullpage.moveTo(3);
			}
		});
	}, false);

})


// function typeWorks() {
// 	var typed3 = new Typed('#typing-works', {
// 		strings: ['ACTION <br class="title-new-line">SPEAKS <br>LOUDEST'],
// 		typeSpeed: 25,
// 		loop: false,
// 	});
// }


$(window).load(function () {
	// setTimeout(function () {
	// 	typeWorks();
	// }, 2100);
	setTimeout(function () {
		$('.animation-in').css('display', 'block').addClass('animated fadeInUp');
	}, 2600);

});